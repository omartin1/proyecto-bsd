﻿using System.Collections.Generic;
using Modelo;

namespace RepositorioCore
{
  public interface IRepositorio
  {
    List<Moneda> ListaMonedas { get; set; }
    List<FactorConversion> ListaFactores { get; set; }
    List<Pais> ListaPaises { get; set; }
    void ActualizarMoneda(Moneda moneda);
    void ActualizarFactor(FactorConversion factor);
    void ActualizarPais(Pais pais);
    void BorrarMoneda(int id);
    void BorrarFactor(int id);
    void BorrarPais(int id);
    Moneda BuscarMonedaPorId(int idMoneda);
    FactorConversion BuscarFactorPorId(int idFactor);
    Pais BuscarPaisPorId(int idPais);
    int BuscarMonedaPorSiglas(string siglaMoneda);
    int BuscarFactorPorMonedas(int origen, int destino);
    int BuscarPaisPorNombre(string nombre);
    void CrearMoneda(Moneda moneda);
    void CrearFactor(FactorConversion factor);
    void CrearPais(Pais pais);
    List<Moneda> GetMonedas();
    List<FactorConversion> GetFactores();
    List<Pais> GetPaises();
    List<Moneda> ObtenerMonedas();
    List<FactorConversion> ObtenerFactores();
    List<Pais> ObtenerPaises();
  }
}