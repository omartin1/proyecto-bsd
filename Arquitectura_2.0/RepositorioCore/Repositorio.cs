﻿using System.Collections.Generic;
using System.Linq;
using Contexto;
using Modelo;

namespace RepositorioCore
{
  public class Repositorio : IRepositorio
  {
    private readonly MonedaDb _contexto;

    public Repositorio(MonedaDb contexto)
    {
      _contexto = contexto;
      ListaMonedas = new List<Moneda>();
      ListaFactores = new List<FactorConversion>();
    }

    // U - UPDATE
    public void ActualizarMoneda(Moneda moneda)
    {
      var buscarMoneda = BuscarMonedaPorId(BuscarMonedaPorSiglas(moneda.IdentificadorMoneda));
      if (buscarMoneda != null)
      {
        buscarMoneda.Nombre = moneda.Nombre;
        buscarMoneda.IdentificadorMoneda = moneda.IdentificadorMoneda;
        _contexto.SaveChanges();
      }

    }



    // D - DELETE

    public void BorrarMoneda(int id)
    {
      var buscarMoneda = BuscarMonedaPorId(id);
      if (buscarMoneda == null) return;
      _contexto.Monedas.Remove(buscarMoneda);
      _contexto.SaveChanges();
    }


    public Moneda BuscarMonedaPorId(int IdMoneda)
    {
      return _contexto.Monedas.FirstOrDefault(
        p => p.Id == IdMoneda);
    }


    // C - CREATE

    public void CrearMoneda(Moneda moneda)
    {
      var buscarMoneda = BuscarMonedaPorSiglas(moneda.IdentificadorMoneda);
      // Comprueba si ha encontrado la moneda
      if (buscarMoneda != -1)
      {
        // Ha encontrado la moneda
        // La actualizamos
        ActualizarMoneda(moneda);
      }
      else
      {
        // No ha encontrado la moneda
        // Creamos la moneda
        _contexto.Monedas.Add(moneda);
        _contexto.SaveChanges();
      }

    }

    public List<Moneda> GetMonedas()
    {
      return new List<Moneda>();
    }

    public List<Moneda> ListaMonedas { get; set; }
    public List<FactorConversion> ListaFactores { get; set; }
    public List<Pais> ListaPaises { get => throw new System.NotImplementedException(); set => throw new System.NotImplementedException(); }

    // R - RETRIEVE
    public List<Moneda> ObtenerMonedas()
    {

      return _contexto.Monedas.ToList();
    }

    public void ActualizarFactor(FactorConversion factor)
    {
      var buscarFactor = BuscarFactorPorId(factor.Id);
      if (buscarFactor != null)
      {
        buscarFactor.Factor = factor.Factor;
        buscarFactor.IdMonedaOrigen = factor.IdMonedaOrigen;
        buscarFactor.IdMonedaDestino = factor.IdMonedaDestino;
        _contexto.SaveChanges();
      }
    }

    public void BorrarFactor(int id)
    {
      var buscarFactor = BuscarFactorPorId(id);
      if (buscarFactor == null) return;
      _contexto.FactoresConverion.Remove(buscarFactor);
      _contexto.SaveChanges();
    }

    public FactorConversion BuscarFactorPorId(int idFactor)
    {
      return _contexto.FactoresConverion.FirstOrDefault(
    p => p.Id == idFactor);
    }

    public void CrearFactor(FactorConversion factor)
    {
      var buscarFactor = BuscarFactorPorId(BuscarFactorPorMonedas(factor.IdMonedaOrigen,factor.IdMonedaDestino));
      // Comprueba si ha encontrado la moneda
      if (buscarFactor != null)
      {
        // Ha encontrado la moneda
        // La actualizamos
        factor.Id = buscarFactor.Id;
        ActualizarFactor(factor);
      }
      else
      {
        // No ha encontrado la moneda
        // Creamos la moneda
        _contexto.FactoresConverion.Add(factor);
        _contexto.SaveChanges();
      }
    }

    public List<FactorConversion> GetFactores()
    {
      return new List<FactorConversion>();
    }

    public List<FactorConversion> ObtenerFactores()
    {
      return _contexto.FactoresConverion.ToList();
    }

    public int BuscarMonedaPorSiglas(string siglaMoneda)
    {
      var aux = _contexto.Monedas.FirstOrDefault(
   p => p.IdentificadorMoneda == siglaMoneda);
      if (aux == null)
        return -1;
      return aux.Id;
    }

    public int BuscarFactorPorMonedas(int origen, int destino)
    {
      var aux = _contexto.FactoresConverion.FirstOrDefault(
    p => (p.IdMonedaOrigen == origen) && (p.IdMonedaDestino == destino));
      if (aux == null)
        return -1;
      return aux.Id;
    }

    public void ActualizarPais(Pais pais)
    {
      var buscarPais = BuscarPaisPorId(BuscarPaisPorNombre(pais.NombrePais));
      if (buscarPais != null)
      {
        buscarPais.NombrePais = pais.NombrePais;
        _contexto.SaveChanges();
      }
    }

    public void BorrarPais(int id)
    {
      var buscarPais = BuscarPaisPorId(id);
      if (buscarPais == null) return;
      _contexto.Paises.Remove(buscarPais);
      _contexto.SaveChanges();
    }

    public Pais BuscarPaisPorId(int idPais)
    {
      return _contexto.Paises.FirstOrDefault(
    p => p.Id == idPais);
    }

    public int BuscarPaisPorNombre(string nombre)
    {
      var aux = _contexto.Paises.FirstOrDefault(
p => p.NombrePais == nombre);
      if (aux == null)
        return -1;
      return aux.Id;
    }

    public void CrearPais(Pais pais)
    {
      var buscarPais = BuscarPaisPorNombre(pais.NombrePais);
      // Comprueba si ha encontrado la moneda
      if (buscarPais != -1)
      {
        // Ha encontrado la moneda
        // La actualizamos
        ActualizarPais(pais);
      }
      else
      {
        // No ha encontrado la moneda
        // Creamos la moneda
        _contexto.Paises.Add(pais);
        _contexto.SaveChanges();
      }
    }

    public List<Pais> GetPaises()
    {
      return new List<Pais>();
    }

    public List<Pais> ObtenerPaises()
    {
      return _contexto.Paises.ToList();
    }
  }
}