﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Modelo;
using RepositorioCore;

namespace CityInfo.API.Controllers
{
  [Route("api/Factores")]
  public class FactorComando : Controller
  {
    private readonly IRepositorio _repositorio;
    public FactorComando(IRepositorio repositorio)
    {
      _repositorio = repositorio;
    }
    //[HttpGet()]
    //public IActionResult GetHola()
    //{
    //  return Ok("hola mundo bonito");
    //}
    [HttpPost()]
    public IActionResult AddFactor([FromBody] FactorConversion factor)
    {
      _repositorio.CrearFactor(factor);
      return Ok(factor);
    }
    [HttpGet()]
    public IActionResult GetFactores()
    {
      return Ok(_repositorio.ObtenerFactores());
    }
    [HttpGet("{idOrigen}/{idDestino}")]
    public IActionResult GetIdFactorPorMonedas(int idOrigen, int idDestino)
    {
      return Ok(_repositorio.BuscarFactorPorMonedas(idOrigen, idDestino));
    }
    [HttpGet("{id}")]
    public IActionResult GetFactorPorId(int id)
    {
      return Ok(_repositorio.BuscarFactorPorId(id));
    }
    [HttpPut("{id}")]
    public IActionResult UpdateFactor(int id,[FromBody] FactorConversion factor)
    {
      factor.Id = id;
      _repositorio.ActualizarFactor(factor);
      return Ok(factor);
    }
    [HttpDelete("{id}")]
    public IActionResult DeleteFactor(int id)
    {
      _repositorio.BorrarFactor(id);
      return Ok(id);
    }
  }
}
