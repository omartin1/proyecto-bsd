﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Modelo;
using RepositorioCore;
using SitioCore.Models;
using SitioWeb.ViewModels;
using ForexQuotes;
using Forex;
using Microsoft.AspNetCore.Authorization;

namespace SitioCore.Controllers
{
  public class HomeController : Controller
  {
    private readonly IRepositorio _repositorio;

    public HomeController(IRepositorio repositorio)
    {
      _repositorio = repositorio;
    }
    [Authorize]
    public IActionResult VerMonedas()
    {
      var listaMonedas = _repositorio.ObtenerMonedas();

      var homeViewModel = new HomeViewModel
      {
        Titulo = "Calculin",
        ListaMonedas = listaMonedas,
        ImagenMoneda = "https://www.worldatlas.com/r/w728-h425-c728x425/upload/d0/91/86/shutterstock-403371907.jpg"
      };

      return View(homeViewModel);
    }

    public IActionResult ActualizarMonedas()
    {
      var client = new ForexDataClient("VTD5wO9aKCwf8BHUSsP2C7i6ROs1DvfH");
      var symbol = client.GetSymbols();
      var valores = client.GetQuotes(symbol);
      foreach (var sym in symbol)
      {
        var aux = sym.Substring(0, 3);
        _repositorio.CrearMoneda(new Moneda { IdentificadorMoneda = aux });
      }
      foreach (var valor in valores)
      {
        var aux = valor.symbol;
        var auxOrigen = _repositorio.BuscarMonedaPorSiglas(aux.Substring(0, 3));
        var auxDestino = _repositorio.BuscarMonedaPorSiglas(aux.Substring(3, 3));
        _repositorio.CrearFactor(new FactorConversion { IdMonedaOrigen = auxOrigen, IdMonedaDestino = auxDestino, Factor = (decimal)valor.price });
      }
      return View();
    }

    public IActionResult Index()
    { 
      return View();
    }
    [Authorize]
    public IActionResult DetalleMoneda(int id)
    {
      var moneda = _repositorio.BuscarMonedaPorId(id);
      if (moneda == null)
        return NotFound();

      return View(moneda);
    }
    [Authorize]
    public IActionResult Calcular(HomeViewModel model)
    {
      var res = model.Cantidad;
      var IdOrigen = _repositorio.BuscarMonedaPorSiglas(model.IDorigen);
      var IdDestino = _repositorio.BuscarMonedaPorSiglas(model.IDdestino);
      var IdFactor = _repositorio.BuscarFactorPorMonedas(IdOrigen, IdDestino);

      var factor = _repositorio.BuscarFactorPorId(IdFactor);
      if(factor != null)
      {
        res = model.Cantidad * factor.Factor;
      }
      //var moneda = _repositorio.BuscarMonedaPorId(id);
      //if (moneda == null)
      //  return NotFound();
      var homeViewModern = new HomeViewModern
      {
        Resultado = res,
        Cantidad = model.Cantidad,
        MonedaOrigen = model.IDorigen,
        MonedaDestino = model.IDdestino
      };

      return View(homeViewModern);
    }



    [HttpPost]
    public IActionResult DetalleMoneda(Moneda moneda)
    {
      if (ModelState.IsValid)
      {
        _repositorio.ActualizarMoneda(moneda);
        return RedirectToAction("VerMonedas");
      }
      return View(moneda);
    }

    public IActionResult About()
    {
      ViewData["Message"] = "Your application description page.";

      return View();
    }
    [Authorize]
    public IActionResult Contact()
    {
      ViewData["Message"] = "Your contact page.";

      return View();
    }

    public IActionResult Privacy()
    {
      return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
      return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
  }
}
