﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using RepositorioCore;
using SitioCore.Data;
using SitioCore.Models;

namespace SitioCore.Controllers
{
  public class AccountController : Controller
  {
    private readonly UserManager<UsuarioConversor> userManager;
    private readonly SignInManager<UsuarioConversor> signInManager;
    private readonly RoleManager<IdentityRole> roleManager;
    private readonly IRepositorio _repositorio;

    public AccountController(UserManager<UsuarioConversor> userManager, SignInManager<UsuarioConversor> signInManager, RoleManager<IdentityRole> roleManager,IRepositorio repositorio)
    {
      this.userManager = userManager;
      this.signInManager = signInManager;
      this.roleManager = roleManager;
      _repositorio = repositorio;
  }

    [HttpGet]
    public IActionResult Register()
    {
      var listaPaises = _repositorio.ObtenerPaises();

      var homeViewModel = new RegisterViewModel
      {
        ListaPaises = listaPaises
      };

      return View(homeViewModel);
    }

    [HttpPost]
    public async Task<IActionResult> Register(RegisterViewModel model)
    {
      if (!ModelState.IsValid)
        return View("Error");

      var user = new UsuarioConversor() { UserName = model.Nombre, Email = model.Email, FechaNacimiento = model.FechaNacimiento, IdPais = model.IdPais };
      var result = await userManager.CreateAsync(
          user, model.Password);

      //await userManager.AddClaimAsync(user, new Claim("IdPais", model.IdPais));

      if (result.Succeeded)
        return View("RegistrationConfirmation");

      foreach (var error in result.Errors)
        ModelState.AddModelError("error", error.Description);
      return View(model);
    }
    [HttpGet]
    public IActionResult Login(string returnUrl = null)
    {
      ViewData["ReturnUrl"] = returnUrl;
      return View();
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
    {
      ViewData["ReturnUrl"] = returnUrl;
      if (ModelState.IsValid)
      {
        var result =
            await
                signInManager.PasswordSignInAsync(model.Nombre, model.Password, model.RememberMe,
                    lockoutOnFailure: false);
        if (result.Succeeded)
          return RedirectToLocal(returnUrl);
        if (result.RequiresTwoFactor)
        {
          //
        }
        if (result.IsLockedOut)
        {
          return View("Lockout");
        }

        ModelState.AddModelError(string.Empty, "Invalid login attempt.");
        return View(model);

      }
      return View(model);
    }

    [HttpGet]
    public async Task<IActionResult> LogOff()
    {
      await signInManager.SignOutAsync();
      return RedirectToAction("Index", "Home/Index");
      //return View("LoggedOut");
    }

    private IActionResult RedirectToLocal(string returnUrl)
    {
      if (Url.IsLocalUrl(returnUrl))
      {
        return Redirect(returnUrl);
      }
      return RedirectToAction("Index", "Home/VerMonedas");
    }

  }
}
