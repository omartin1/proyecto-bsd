﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Modelo;
using RepositorioCore;
using Microsoft.Extensions.Logging;

namespace CityInfo.API.Controllers
{
  [Route("api/Monedas")]
  public class MonedaComando : Controller
  {
    private readonly IRepositorio _repositorio;
    private ILogger<MonedaComando> _logger;
    public MonedaComando(IRepositorio repositorio, ILogger<MonedaComando> logger)
    {
      _repositorio = repositorio;
      _logger = logger;
    }
    //[HttpGet()]
    //public IActionResult GetHola()
    //{
    //  return Ok("hola mundo bonito");
    //}
    [HttpPost()]
    public IActionResult AddMoneda([FromBody] Moneda moneda)
    {
      _repositorio.CrearMoneda(moneda);
      return Ok(moneda);
    }
    [HttpGet()]
    public IActionResult GetMonedas()
    {
      var monedas = _repositorio.ObtenerMonedas();
      return Ok(monedas);
    }
    [HttpGet("porNombre/{nombre}")]
    public IActionResult GetIdMonedaPorNombre(string nombre)
    {
      var Idmoneda = _repositorio.BuscarMonedaPorSiglas(nombre);
      if (Idmoneda==-1)
        _logger.LogInformation($"La moneda de nombre {nombre} no se ha encontrado");
      return Ok(Idmoneda);
    }
    [HttpGet("{id}")]
    public IActionResult GetMonedaPorId(int id)
    {
      var moneda = _repositorio.BuscarMonedaPorId(id);
      if (moneda== null)
        _logger.LogInformation($"La moneda de id {id} no se ha encontrado");
      return Ok(moneda);
    }
    [HttpPut("{id}")]
    public IActionResult UpdateMoneda(int id,[FromBody] Moneda moneda)
    {
      moneda.Id = id;
      _repositorio.ActualizarMoneda(moneda);
      return Ok(moneda);
    }
    [HttpDelete("{id}")]
    public IActionResult DeleteMoneda(int id)
    {
      _repositorio.BorrarMoneda(id);
      return Ok(id);
    }
  }
}
