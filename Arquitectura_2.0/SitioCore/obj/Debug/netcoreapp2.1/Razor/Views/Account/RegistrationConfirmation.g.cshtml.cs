#pragma checksum "C:\Users\usuario\Desktop\Arquitectura_2.0\Arquitectura_2.0\SitioCore\Views\Account\RegistrationConfirmation.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "1efff129cdd098f8f6d2c22b20fba216a1f653db"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Account_RegistrationConfirmation), @"mvc.1.0.view", @"/Views/Account/RegistrationConfirmation.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Account/RegistrationConfirmation.cshtml", typeof(AspNetCore.Views_Account_RegistrationConfirmation))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\usuario\Desktop\Arquitectura_2.0\Arquitectura_2.0\SitioCore\Views\_ViewImports.cshtml"
using SitioCore;

#line default
#line hidden
#line 2 "C:\Users\usuario\Desktop\Arquitectura_2.0\Arquitectura_2.0\SitioCore\Views\_ViewImports.cshtml"
using SitioCore.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"1efff129cdd098f8f6d2c22b20fba216a1f653db", @"/Views/Account/RegistrationConfirmation.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"2be238dc68979822c90a4857d30e68fcbee35991", @"/Views/_ViewImports.cshtml")]
    public class Views_Account_RegistrationConfirmation : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 1 "C:\Users\usuario\Desktop\Arquitectura_2.0\Arquitectura_2.0\SitioCore\Views\Account\RegistrationConfirmation.cshtml"
  
    ViewData["Title"] = "Registration Confirmation";

#line default
#line hidden
            BeginContext(61, 6, true);
            WriteLiteral("\r\n<h2>");
            EndContext();
            BeginContext(68, 17, false);
#line 5 "C:\Users\usuario\Desktop\Arquitectura_2.0\Arquitectura_2.0\SitioCore\Views\Account\RegistrationConfirmation.cshtml"
Write(ViewData["Title"]);

#line default
#line hidden
            EndContext();
            BeginContext(85, 48, true);
            WriteLiteral(".</h2>\r\n<p>\r\n    Thanks for registering!\r\n</p>\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
