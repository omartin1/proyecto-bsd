﻿using System.Collections.Generic;
using Modelo;

namespace SitioWeb.ViewModels
{
  public class HomeViewModern
  {
    public decimal Resultado { get; set; }
    public decimal Cantidad { get; set; }
    public string MonedaOrigen { get; set; }
    public string MonedaDestino { get; set; }
  }
}