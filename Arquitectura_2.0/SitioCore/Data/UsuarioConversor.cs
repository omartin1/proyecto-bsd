﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace SitioCore.Data
{
  public class UsuarioConversor : IdentityUser
  {
    public DateTime FechaNacimiento { get; set; }
    public int IdPais { get; set; }

  }
}
